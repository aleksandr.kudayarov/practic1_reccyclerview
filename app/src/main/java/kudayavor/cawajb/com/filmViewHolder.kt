package kudayavor.cawajb.com

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kudayavor.cawajb.com.databinding.ItemFilmBinding

class filmViewHolder(private val itemFilmBinding:ItemFilmBinding) : RecyclerView.ViewHolder(itemFilmBinding.root) {
    fun bind(film:Film) = with(itemFilmBinding){
        filmName.text = film.name
        filmRating.text = film.rating
        filmDate.text = film.date
        filmDescription.text = film.description
        filmImage.setImageDrawable(film.logo)


    }
}