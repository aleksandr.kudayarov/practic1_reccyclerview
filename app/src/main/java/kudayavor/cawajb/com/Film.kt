package kudayavor.cawajb.com

import android.graphics.drawable.Drawable

data class Film(
    val rating: String,
    val name: String,
    val date: String,
    val description: String,
    val logo: Drawable?
) {
}