package kudayavor.cawajb.com

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kudayavor.cawajb.com.databinding.ItemFilmBinding

class FilmsAdapter(private val films: MutableList<Film>) : RecyclerView.Adapter<filmViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): filmViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ItemFilmBinding.inflate(layoutInflater)
        return filmViewHolder(binding)

    }

    override fun getItemCount(): Int {
        return films.size
    }

    override fun onBindViewHolder(holder: filmViewHolder, position: Int) {
        val currentFilm = films[position]
        holder.bind(currentFilm)
    }
}