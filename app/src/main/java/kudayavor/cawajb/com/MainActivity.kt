package kudayavor.cawajb.com

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import kudayavor.cawajb.com.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private val films = mutableListOf<Film>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        inflateFilms()

        val adapterFilms = FilmsAdapter(films)
        binding.recyclerFilms.layoutManager = GridLayoutManager(this, 1)
        binding.recyclerFilms.adapter = adapterFilms
    }

    private fun inflateFilms(){
        films.clear()
        films.add(
            Film(
                "7.6","Убить Билла", "2003 год", "Наемная убийца жестоко мстит бывшим подельникам. Жестокий боевик Квентина Тарантино — с анимацией и отсылками",
                ContextCompat.getDrawable(this, R.drawable.kill_bill)
            )
        )
        films.add(
            Film(
                "7.5","Убить Билла 2", "2004 год", "Черная Мамба все ближе к главарю банды. Продолжение синефильского экшена Квентина Тарантино",
                ContextCompat.getDrawable(this, R.drawable.kill_bill2)
            )
        )
        films.add(
            Film(
                "8.1","Последний самурай", "2004 год", "Пытаясь подавить восстание в Японии, Том Круз сам попадает в плен. Зрелищная военная притча о памяти и чести",
                ContextCompat.getDrawable(this, R.drawable.last_samurai)
            )
        )
        films.add(
            Film(
                "8.0","Троя", "2004 год", "Зрелищная экранизация «Илиады» с шикарными костюмами и масштабными битвами. Брэд Питт в роли могучего Ахиллеса",
                ContextCompat.getDrawable(this, R.drawable.troy)
            )
        )
        films.add(
            Film(
                "8.0","Джокер", "2019 год", "Как неудачливый комик стал самым опасным человеком в Готэме. Бенефис Хоакина Феникса и «Оскар» за саундтрек",
                ContextCompat.getDrawable(this, R.drawable.joker)
            )
        )
    }
}